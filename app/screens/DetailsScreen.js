import React from "react";
import {
  Container,
  Content,
  Text,
} from "native-base";

function DetailsScreen ({ route, navigation }) {
    const { word } = route.params;
    return (
      <Container>
        <Content>
          <Text>word: {JSON.stringify(word.word)}</Text>
          <Text>id: {JSON.stringify(word.id)}</Text>
          <Text>isFavorite: {JSON.stringify(word.isFavorite)}</Text>
        </Content>
      </Container>
    );
}

export default DetailsScreen
