import React from "react";
import {
  Container,
  Header,
  Content,
  Text,
  Icon,

  Item,
  Input,
} from "native-base";

export default class FavoritesScreen extends React.Component {
  state = {
    
  }

render() {
    return (
        <Container>
        <Header searchBar rounded style={{backgroundColor: '#fff'}}>
            <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" />
            </Item>
        </Header>
        <Content>
            <Text>Favorites</Text>
        </Content>
        </Container>
    );
  }
}
