import React from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Icon,
  Body,
  Right,
  Item,
  Input,
} from "native-base";

export default class DictionaryScreen extends React.Component {

  componentDidMount () {
    this.setState({words: this.response});
  };

  state = {
    words: [],
  }

  response = [
      { id: 1, word: "Baba", isFavorite: false },
      { id: 2, word: "Nena", isFavorite: false },
      { id: 3, word: "Peshk", isFavorite: false },
      { id: 4, word: "Top", isFavorite: false },
      { id: 5, word: "Futboll", isFavorite: false },
      { id: 6, word: "Tenis", isFavorite: true },
  ]; 

  handleFavoriteClick = (word) => {
    let words = [...this.state.words];

    words.forEach(w => {
      if (w.id === word.id) {
        w.isFavorite = !w.isFavorite;
      }
    })

    this.setState({words: words});
  };

  searchFilterFunction = (text) => {
    const newData = this.response.filter(item => {      
      const itemData = `${item.word.toLowerCase()}`;
      
       const textData = text.toLowerCase();
        
       return itemData.indexOf(textData) > -1;    
    });
    
    this.setState({ words: newData });
  };

render() {
    return (
        <Container>
        <Header searchBar rounded style={{backgroundColor: '#fff'}}>
            <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" onChangeText={text => this.searchFilterFunction(text)} autoCorrect={false} />
            </Item>
        </Header>
        <Content>
            <List>
            {this.state.words.map((word) => {
                return (
                <ListItem key={word.id} onPress={ () => {this.props.navigation.navigate('Details', {
                  word: word,
                }) }}>
                    <Body>
                    <Text>{word.word}</Text>
                    </Body>
                    <Right>
                    <Icon
                        style={{ color: word.isFavorite ? "orange" : "silver" }}
                        name="star"
                        onPress={() => this.handleFavoriteClick(word)}
                    />
                    </Right>
                </ListItem>
                );
            })}
            </List>
        </Content>
        </Container>
    );
  }
}
