import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from "native-base";

import DictionaryScreen from './DictionaryScreen';
import SettingsScreen from './SettingsScreen';
import FavoritesScreen from './FavoritesScreen';

export default class MainScreen extends React.Component {

 
render() {
  const Tab = createBottomTabNavigator();

    return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused }) => {
            let iconName;

            switch (route.name) {
              case 'Dictionary':
                iconName = focused
                ? 'book'
                : 'book-outline';
                break;
              case 'Favorites':
                iconName = focused ? 'star' : 'star-outline';
                break;
              case 'Settings':
                iconName = focused ? 'settings' : 'settings-outline';
                break;
            
              default:
                break;
            }

            // You can return any component that you like here!
            return <Icon active name={iconName} style={{ color: 'gray' }}/>;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'red',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Dictionary" component={DictionaryScreen} />
        <Tab.Screen name="Favorites" component={FavoritesScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    );
  }
}
